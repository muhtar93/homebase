import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  containerStyle: {
    flex: 1
  },
  containerItemStyle: {
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 20,
    elevation: 2,
    paddingBottom: 8,
    paddingTop: 4
  }
})

export default Styles