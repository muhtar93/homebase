import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    backgroundColor: 'white'
  }
})

export default Styles