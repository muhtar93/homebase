import React, { Component } from 'react'
import { View, Text, Alert } from 'react-native'
import Header from '../../components/Header'
import Styles from './Styles'
import TabBar from '../../components/TabBar'
import Listing from '../listing/Listing'
import Favorite from '../favorite/Favorite'
import Arsip from '../arsip/Arsip'
import firebase from 'react-native-firebase'
import AsyncStorage from '@react-native-community/async-storage'

const TabIndex = {
  LISTING: 0,
  FAVORITE: 1,
  ARSIP: 2
}

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tabIndex: TabIndex.LISTING
    }
  }

  componentDidMount() {
    this.checkPermission()
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getFcmToken()
      this.messageListener()
    } else {
      this.requestPermission();
    }
  }

  getFcmToken = async () => {
    const fcmToken = await firebase.messaging().getToken()
    if (fcmToken) {
      console.log(fcmToken)
      this.saveKey(fcmToken)
    } else {
      Alert.alert('Failed', 'No token received');
    }
  }

  async saveKey(fcmToken) {
    try {
      await AsyncStorage.setItem('token', fcmToken)
    } catch (error) {
      console.log('Error saving token', error)
    }
  }

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      this.messageListener()
    } catch (error) {
      Alert.alert(
        "Perhatian",
        "Anda harus mengizinkan permintaan notifikasi",
        [
          {
            text: "OK", onPress: () => Linking.openURL('app-settings:'),
            style: "cancel"
          }
        ],
        { cancelable: false }
      )
    }
  }

  messageListener = async () => {

    const channel = new firebase.notifications.Android.Channel(
      'channelId',
      'Channel Name',
      firebase.notifications.Android.Importance.Max
    ).setDescription('A natural description of the channel');
    firebase.notifications().android.createChannel(channel);

    this.notificationListener = await firebase.notifications().onNotification((notification) => {
      const localNotification = new firebase.notifications.Notification({
        sound: 'default',
        show_in_foreground: true,
      })
        .setNotificationId(notification.notificationId)
        .setTitle(notification.title)
        .setSubtitle(notification.subtitle)
        .setBody(notification.body)
        .setData(notification.data)
        .android.setChannelId('channelId') // e.g. the id you chose above
        .android.setSmallIcon('ic_launcher') // create this icon in Android Studio
        .android.setColor('#00b200') // you can set a color here
        .android.setAutoCancel(true) // Njay bisa juga ini buat hapus nya ternyata
      //.android.setPriority(firebase.notifications.Android.Priority.High);

      firebase.notifications()
        .displayNotification(localNotification)
        .catch(err => console.error(err))
      firebase.notifications().removeDeliveredNotification(notification.notificationId)
      // console.log('notification ', notification)
    })


    /* Konidisi jika aplikasi sedang running background */
    this.notificationOpenedListener = await firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
    })

    /* Konidisi jika aplikasi di tutup (kill) */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      // const { title, body } = notificationOpen.notification;
      // console.log('getInitialNotification.notification', notificationOpen.notification)
      // //this.showAlert(title, body)
      // if (notificationOpen.notification._data.tipe === 'PesanBantuanOperator') {
      //   navigation.navigate('BantuanOperator')
      //   this.props.onReadMessage(this.state.token)
      // } else if (notificationOpen.notification._data.tipe === 'Pengumuman') {
      //   navigation.navigate('Pengumuman')
      // } else if (notificationOpen.notification._data.tipe === 'ResetDevice') {
      //   // this.logout()
      // } else {
      //   navigation.navigate('Notifikasi')
      // }

      // Get information about the notification that was opened
      const { title, body } = notificationOpen.notification;
      console.log('getInitialNotification.notification', notificationOpen.notification)
    }


    /*Triggered for data only payload in foreground */
    this.messageListener = await firebase.messaging().onMessage((message) => {
      // console.log('crot', message);
    });
  }

  render() {
    let { tabIndex } = this.state
    let { navigation } = this.props

    return (
      <View style={Styles.containerStyle}>
        <Header />
        <TabBar
          items={[
            {
              index: TabIndex.LISTING,
              label: 'Listing'
            },
            {
              index: TabIndex.FAVORITE,
              label: 'Favorite'
            },
            {
              index: TabIndex.ARSIP,
              label: 'Arsip'
            }
          ]}
          onPress={(index) => {
            this.setState({
              tabIndex: index
            })
          }}
        />
        {(() => {
          switch (tabIndex) {
            case TabIndex.LISTING:
              return <Listing navigation={navigation} />
            case TabIndex.FAVORITE:
              return <Favorite />
            case TabIndex.ARSIP:
              return <Arsip />
            default:
              return <Listing />
          }
        })()}
      </View>
    )
  }
}
