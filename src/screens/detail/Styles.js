import { StyleSheet } from 'react-native'
import Fonts from '../../consts/Fonts'

const Styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    backgroundColor: '#E0E0E0',
    opacity: 1
  },
  imageStyle: {
    height: 200,
    width: '100%',
    marginTop: 8
  },
  buttonStyle: {
    backgroundColor: 'black',
    margin: 16,
    height: 45,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    color: 'white',
    fontFamily: Fonts.AvenirNextLTProDemi,
    marginLeft: 8,
    alignSelf: 'center'
  },
  containerButtonStyle: {
    backgroundColor: '#E0E0E0'
  },
  shareStyle: {
    width: 20,
    height: 20,
    tintColor: 'white'
  },
  containerShareStyle: {
    flexDirection: 'row'
  },
  cardOneStyle: {
    backgroundColor: 'white',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8
  },
  nameStyle: {
    fontFamily: Fonts.AvenirNextLTProRegular,
    marginTop: 8,
    marginLeft: 8
  },
  priceStyle: {
    fontFamily: Fonts.AvenirNextLTProBold,
    marginLeft: 8,
    fontSize: 20,
    marginTop: 8
  },
  iconMapStyle: {
    width: 60,
    height: 40,
    marginRight: 8,
    marginTop: 4,
    borderRadius: 8
  }
})

export default Styles