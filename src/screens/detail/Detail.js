import React, { Component } from 'react'
import { View, Text, ScrollView, Image, TouchableOpacity, FlatList } from 'react-native'
import HeaderDetail from '../../consts/HeaderDetail'
import Styles from './Styles'
import axios from 'axios'
import Line from '../../components/Line'
import Fonts from '../../consts/Fonts'
import Colors from '../../consts/Colors'
import Button from '../../components/Button'
import TabBar from '../../components/TabBar'
import { connect } from 'react-redux'
import * as actions from '../../actions'
import AsyncStorage from '@react-native-community/async-storage'

const TabIndex = {
  LISTING_INFO: 0,
  DESKRIPSI: 1
}

const DATA = [
  {
    id: 1,
    name: 'Kamar Tidur',
    size: '3+1'
  },
  {
    id: 2,
    name: 'Kamar Mandi',
    size: '3+1'
  },
  {
    id: 3,
    name: 'Luas Bangunan',
    size: '250M²'
  },
  {
    id: 4,
    name: 'Luas Tanah',
    size: '300M²'
  },
  {
    id: 6,
    name: '(P x L) Bangunan',
    size: '25 x 10M'
  },
  {
    id: 6,
    name: '(P x L) Tanah',
    size: '30 x 10M'
  },
  {
    id: 7,
    name: 'Jumlah Lantai',
    size: '2'
  },
  {
    id: 8,
    name: 'Listrik',
    size: '3000W'
  },
  {
    id: 9,
    name: 'Kapasitas Garasi',
    size: '2'
  },
  {
    id: 10,
    name: 'Kapasitas Parkir',
    size: '2'
  },
  {
    id: 11,
    name: 'Line Telepon',
    size: '2'
  }
]

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: TabIndex.LISTING_INFO,
      token: ''
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('token').then((value) => {
      console.log('token', value)
      this.setState({
        token: value
      })
    })
  }

  render() {
    let { navigation } = this.props

    return (
      <View style={Styles.containerStyle}>
        <HeaderDetail onPress={() => navigation.goBack()} />
        <ScrollView>
          {/* Card 1 */}
          <View style={Styles.cardOneStyle}>
            <Image
              source={require('../../images/property_1.png')}
              style={Styles.imageStyle}
              resizeMode='cover'
            />
            <Text style={Styles.nameStyle}>Nava Park BSD City</Text>
            <Text style={Styles.priceStyle}>Rp 6.500.000.000</Text>
            <Line />
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 8 }}>
              <View>
                <Text style={{ marginLeft: 8, marginTop: 4, fontFamily: Fonts.AvenirNextLTProRegular }}>Rumah untuk Dijual</Text>
                <Text style={{ marginLeft: 8, marginTop: 4, fontFamily: Fonts.AvenirNextLTProRegular, fontSize: 12 }}>Jl. Edutown Kav III.1, BSD, Tangerang Selatan</Text>
              </View>
              <Image
                source={require('../../images/map_detail.png')}
                style={Styles.iconMapStyle}
                resizeMode='cover'
              />
            </View>
          </View>
          {/* Card 2 */}
          <View style={{ paddingVertical: 16, elevation: 2, borderBottomWidth: 0.5, marginTop: 8, borderColor: Colors.ACTIVE_TAB, borderTopWidth: 0.5, backgroundColor: 'white' }}>
            <TabBar
              items={[
                {
                  index: TabIndex.LISTING_INFO,
                  label: 'Listing Info'
                },
                {
                  index: TabIndex.DESKRIPSI,
                  label: 'Deskripsi'
                }
              ]}
              onPress={(index) => {
                this.setState({
                  tabIndex: index
                })
              }}
            />
            {(() => {
              switch (this.state.tabIndex) {
                case TabIndex.LISTING_INFO:
                  return (
                    <View>
                      <FlatList
                        data={DATA}
                        renderItem={({ item }) =>
                          <View style={{ padding: 8 }}>
                            <Text style={{ fontFamily: Fonts.AvenirNextLTProRegular, color: Colors.ACTIVE_TAB, fontSize: 12 }}>{item.name}</Text>
                            <Text style={{ fontFamily: Fonts.AvenirNextLTProBold, color: Colors.IN_ACTIVE_TAB, fontSize: 18 }}>{item.size}</Text>
                          </View>
                        }
                        numColumns={2}
                        keyExtractor={item => item.id}
                      />
                    </View>
                  )
                case TabIndex.DESKRIPSI:
                  return (
                    <View>
                      <Text style={{ fontFamily: Fonts.AvenirNextLTProRegular, fontSize: 12, margin: 8 }}>Harga terjangkau oleh semua kalangan ^_^</Text>
                    </View>
                  )
                default:
                  return <View><Text>Listing Info</Text></View>
              }
            })()}
          </View>
          {/* Card 3 */}
          <View style={{ paddingVertical: 16, elevation: 2, borderBottomWidth: 0.5, marginTop: 8, borderColor: Colors.ACTIVE_TAB, borderTopWidth: 0.5, backgroundColor: 'white' }}>
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={require('../../images/lock.png')}
                style={{ width: 20, height: 20, marginLeft: 8 }}
                resizeMode='contain'
              />
              <Text style={{ fontFamily: Fonts.AvenirNextLTProDemi, marginLeft: 4 }}>Catatan Internal</Text>
            </View>
            <Text style={{ color: Colors.ACTIVE_TAB, fontFamily: Fonts.AvenirNextLTProRegular, marginLeft: 8, marginTop: 8 }}>Hanya anda yang dapat melihat informasi ini.</Text>
            <Text style={{ fontFamily: Fonts.AvenirNextLTProRegular, marginLeft: 8, marginTop: 12 }}>Alamat Details:{'\n'}NavaPark Cluster Moonlight Blok 38D No. 55</Text>
          </View>
          {/* Card 4 */}
          <View style={{ paddingVertical: 16, elevation: 2, borderBottomWidth: 0.5, marginTop: 8, borderColor: Colors.ACTIVE_TAB, borderTopWidth: 0.5, backgroundColor: 'white' }}>
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={require('../../images/lock.png')}
                style={{ width: 20, height: 20, marginLeft: 8 }}
                resizeMode='contain'
              />
              <Text style={{ fontFamily: Fonts.AvenirNextLTProDemi, marginLeft: 4 }}>Owner Properti</Text>
            </View>
            <Text style={{ color: Colors.ACTIVE_TAB, fontFamily: Fonts.AvenirNextLTProRegular, marginLeft: 8, marginTop: 8 }}>Hanya anda yang dapat melihat informasi ini.</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ fontFamily: Fonts.AvenirNextLTProRegular, marginLeft: 8, marginTop: 12 }}>Nama:</Text>
              <Text style={{ fontFamily: Fonts.AvenirNextLTProDemi, marginTop: 12 }}> Jois Aprilio</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ fontFamily: Fonts.AvenirNextLTProRegular, marginLeft: 8 }}>No. HP:</Text>
              <Text style={{ fontFamily: Fonts.AvenirNextLTProDemi }}> 081388809999</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 8, justifyContent: 'space-around' }}>
              <Button title='Telepon' source={require('../../images/phone.png')} />
              <Button title='WhatsApp' source={require('../../images/wa.png')} />
            </View>
          </View>
        </ScrollView>
        <View style={Styles.containerButtonStyle}>
          <TouchableOpacity style={Styles.buttonStyle} onPress={() => this.props.onPushNotif(this.state.token)}>
            <View style={Styles.containerShareStyle}>
              <Image
                source={require('../../images/share.png')}
                style={Styles.shareStyle}
              />
              <Text style={Styles.textStyle}>Promosikan</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const mapStateToProps = state => {
  return {
    notification: state.PushNotifReducers
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onPushNotif: (token) => dispatch(actions.onPushNotif(token)),
    resetPushNotif: () => dispatch(actions.resetPushNotif())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail)
