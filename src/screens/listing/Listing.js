import React, { Component } from 'react'
import { View, Text, FlatList, Image, TouchableOpacity } from 'react-native'
import Uri from '../../consts/Uri'
import Styles from './Styles'
import axios from 'axios'
import Fonts from '../../consts/Fonts'

const DATA = [
  {
    id: 1,
    photo: Uri.photo,
    username: 'Henry Scott',
    company: 'Century 21 BSD City',
    image: Uri.photo,
    name: 'Nava Park BSD City',
    price: 'Rp 6.500.000.000',
    type: 'Rumah',
    status: 'Dijual',
    privacy: 'PUBLIC',
    address: 'Jl. Edutown Kav III 1, BSD, Tangerang Selatan',
    bathroom: '3+1',
    bedroom: '3+1',
    area: '300M'
  },
  {
    id: 2,
    photo: Uri.photo,
    username: 'Henry Scott',
    company: 'Century 21 BSD City',
    image: Uri.photo,
    name: 'Woody Residence Foresta',
    price: 'Rp 6.500.000.000',
    type: 'Rumah',
    status: 'Disewa',
    privacy: 'PRIVATE',
    address: 'Jl. Edutown Kav III 1, BSD, Tangerang Selatan',
    bathroom: '3+1',
    bedroom: '3+1',
    area: '300M'
  }
]

export default class Listing extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={Styles.containerStyle}>
        <FlatList
          style={{ marginTop: 8 }}
          data={DATA}
          renderItem={({ item }) =>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Detail')}>
              <View style={Styles.containerItemStyle}>
                <View style={{ flexDirection: 'row', margin: 8, justifyContent: 'space-between' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Image
                      source={{ uri: item.photo }}
                      style={{ width: 40, height: 40, borderRadius: 40 / 2 }}
                    />
                    <View style={{ marginLeft: 8 }}>
                      <Text style={{ fontSize: 16, fontFamily: Fonts.AvenirNextLTProBold }}>{item.username}</Text>
                      <Text style={{ fontSize: 12, fontFamily: Fonts.AvenirNextLTProRegular }}>{item.company}</Text>
                    </View>
                  </View>
                  <TouchableOpacity style={{ alignSelf: 'center' }}>
                    <Image
                      source={require('../../images/ellipsis.png')}
                      style={{ width: 20, height: 20 }}
                    />
                  </TouchableOpacity>
                </View>
                <Image
                  source={require('../../images/property_1.png')}
                  style={{ height: 100, width: '99%', marginLeft: 2 }}
                  resizeMode='cover'
                />
                <Text style={{ marginLeft: 8, marginTop: 4, fontFamily: Fonts.AvenirNextLTProRegular }}>{item.name}</Text>
                <Text style={{ marginLeft: 8, marginTop: 4, fontFamily: Fonts.AvenirNextLTProBold, fontSize: 16 }}>{item.price}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ marginLeft: 8, marginTop: 4, fontFamily: Fonts.AvenirNextLTProDemi }}>{item.type}</Text>
                  <Text style={{ marginLeft: 8, marginTop: 4, fontFamily: Fonts.AvenirNextLTProBold, color: 'white', backgroundColor: item.status === 'Dijual' ? 'blue' : 'green', paddingHorizontal: 8, paddingVertical: 2, borderRadius: 4, fontSize: 12 }}>{item.status}</Text>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 4 }}>
                  <Image
                    source={require('../../images/placeholder.png')}
                    style={{ height: 16, width: 16, marginLeft: 8, tintColor: '#979797' }}
                    resizeMode='contain'
                  />
                  <Text style={{ fontSize: 12, color: '#979797', marginLeft: 4, fontFamily: Fonts.AvenirNextLTProRegular }}>{item.address}</Text>
                </View>
                <View style={{ height: 0.5, backgroundColor: '#979797', marginHorizontal: 8, marginTop: 8 }} />
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Image
                          source={require('../../images/bedroom.png')}
                          style={{ height: 40, width: 40, marginLeft: 8, tintColor: '#979797', marginTop: 8 }}
                          resizeMode='contain'
                        />
                        <Text style={{ marginLeft: 8, color: '#979797', fontFamily: Fonts.AvenirNextLTProRegular }}>K. Tidur</Text>
                      </View>
                      <Text style={{ fontWeight: 'bold', marginTop: 8, fontSize: 24 }}>{item.bedroom}</Text>
                    </View>
                  </View>
                  {/** */}
                  <View style={{ width: 0.5, backgroundColor: '#979797', marginHorizontal: 2, marginTop: 8 }} />
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Image
                          source={require('../../images/bathroom.png')}
                          style={{ height: 40, width: 40, marginLeft: 8, tintColor: '#979797', marginTop: 8 }}
                          resizeMode='contain'
                        />
                        <Text style={{ marginLeft: 8, color: '#979797', fontFamily: Fonts.AvenirNextLTProRegular }}>K. Mandi</Text>
                      </View>
                      <Text style={{ fontWeight: 'bold', marginTop: 8, fontSize: 24 }}>{item.bathroom}</Text>
                    </View>
                  </View>
                  {/** */}
                  <View style={{ width: 0.5, backgroundColor: '#979797', marginHorizontal: 2, marginTop: 8 }} />
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View>
                        <Image
                          source={require('../../images/area.png')}
                          style={{ height: 40, width: 40, marginLeft: 8, tintColor: '#979797', marginTop: 8 }}
                          resizeMode='contain'
                        />
                        <Text style={{ marginLeft: 8, color: '#979797', fontFamily: Fonts.AvenirNextLTProRegular }}>L. Tanah</Text>
                      </View>
                      <Text style={{ marginTop: 16, fontSize: 16, fontFamily: Fonts.AvenirNextLTProBold }}>{item.area}</Text>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          }
          keyExtractor={item => item.id}
        />
      </View>
    )
  }
}
