import React from 'react'
import { View, Text, Dimensions, TouchableOpacity, Animated } from 'react-native'
import Colors from '../consts/Colors'
import Fonts from '../consts/Fonts'

const { width } = Dimensions.get('window')

export default class TabBar extends React.Component {
  index() {
    return this.state.index
  }

  constructor(props) {
    super(props)
    let { items, onPress } = this.props
    for (var i = 0; i < items.length; i++) items[i].index = i
    this.deltaX = width / items.length
    let initialIndex = this.props.initialIndex ? this.props.initialIndex : 0
    this.state = {
      index: initialIndex,
      xPos: new Animated.Value(initialIndex * this.deltaX)
    }
    if (onPress) onPress(initialIndex)
  }

  render() {
    let { index, xPos } = this.state
    let { items, onPress, disableMessage, selectedTextColor, textColor, style } = this.props
    return (
      <View style={[{ width: '100%', height: 46, backgroundColor: 'white', flexDirection: 'row' }, style || null]}>
        {items.map(item =>
          <TouchableOpacity
            key={item.id ? item.id : item.index}
            style={{ flex: 1, height: '100%', justifyContent: 'center', alignItems: 'center' }}
            onPress={() => {
              if (item.isDisabled) {
                setTimeout(() => {
                  Snackbar.show({
                    title: disableMessage || 'Disabled',
                    duration: Snackbar.LENGTH_SHORT
                  })
                }, 100)
                return
              }
              if (onPress) onPress(item.index)
              this.setState({
                index: item.index
              })
              Animated.timing(
                this.state.xPos,
                {
                  toValue: item.index * this.deltaX,
                  duration: 100,
                  useNativeDriver: false
                }
              ).start()
            }}>
            <Text style={{ fontSize: 16, color: item.index === index ? selectedTextColor || Colors.ACTIVE_TAB : textColor || Colors.IN_ACTIVE_TAB, fontFamily: Fonts.AvenirNextLTProDemi }}>{item.label}</Text>
          </TouchableOpacity>
        )}
        <Animated.View style={{ position: 'absolute', bottom: 0, left: xPos }}>
          <View style={{ width: this.deltaX, height: 3, backgroundColor: '#BEAF87' }} />
        </Animated.View>
      </View>
    )
  }
}
