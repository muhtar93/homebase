import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import Fonts from '../consts/Fonts'
import Photo from './Photo'

const Header = () => {
  return (
    <View style={styles.containerStyle}>
      <Photo />
      <Text style={styles.userStyle}>Henry Scott</Text>
      <Text style={styles.memberStyle}>Member Broker Century 21 BSD City</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    height: 150,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
  userStyle: {
    fontFamily: Fonts.AvenirNextLTProBold,
    marginTop: 4
  },
  memberStyle: {
    fontSize: 12,
    fontFamily: Fonts.AvenirNextLTProRegular,
    marginTop: 2
  }
})

export default Header

