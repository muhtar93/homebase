import React from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native'
import Fonts from '../consts/Fonts'

const Button = ({ source, title }) => {
  return (
    <TouchableOpacity style={styles.containerStyle}>
      <View style={{ flexDirection: 'row' }}>
        <Image
          style={{ width: 16, height: 16 }}
          resizeMode='cover'
          source={source}
        />
        <Text style={{ marginLeft: 4, fontFamily: Fonts.AvenirNextLTProRegular }}>{title}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'black',
    elevation: 1,
    padding: 8,
    width: 140
  }
})

export default Button