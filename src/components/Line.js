import React from 'react'
import { View, StyleSheet } from 'react-native'
import Colors from '../consts/Colors'

const Line = () => {
  return (
    <View style={styles.containerStyle} />
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    height: 0.5,
    backgroundColor: Colors.LINE,
    marginHorizontal: 8,
    marginTop: 8
  }
})

export default Line