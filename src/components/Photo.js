import React from 'react'
import { Image, StyleSheet, View, Text } from 'react-native'
import Uri from '../consts/Uri'

const Photo = () => {
  return (
    <View style={styles.containerStyle}>
      <Image
        style={styles.imageStyle}
        source={{ uri: Uri.photo }}
        resizeMode='contain'
      />
    </View>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    width: 95,
    height: 95,
    borderRadius: 95 / 2,
    borderColor: 'grey',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16
  },
  imageStyle: {
    width: 85,
    height: 85,
    borderRadius: 85 / 2
  }
})

export default Photo

