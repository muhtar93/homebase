import Types from '../consts/Types'

const INITIAL_STATE = {
  isLoading: false,
  pushNotif: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.PUSH_NOTIF_LOADING:
      state = {
        ...state,
        isLoading: true
      }
      break
    case Types.PUSH_NOTIF_SUCCESS:
      state = {
        ...state,
        isLoading: false,
        pushNotif: action.result
      }
      break
    case Types.PUSH_NOTIF_ERROR:
      state = {
        ...state,
        isLoading: false,
        pushNotif: action.error
      }
      break
    case Types.PUSH_NOTIF_RESET:
      state = INITIAL_STATE
      break
    default:
      break
  }

  return state
}
