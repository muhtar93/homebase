import { combineReducers } from 'redux'
import PushNotifReducers from './PushNotifReducers'

export default combineReducers({
  PushNotifReducers
})