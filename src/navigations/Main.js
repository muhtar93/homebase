import 'react-native-gesture-handler'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Home from '../screens/home/Home'
import Detail from '../screens/detail/Detail'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import reducers from '../reducers'

const Stack = createStackNavigator()

const Main = () => {
  return (
    <Provider store={createStore(reducers, applyMiddleware(ReduxThunk))}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Home'>
          <Stack.Screen
            name='Home'
            component={Home}
            options={
              { headerShown: false }
            }
          />
          <Stack.Screen
            name='Detail'
            component={Detail}
            options={
              { headerShown: false }
            }
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default Main