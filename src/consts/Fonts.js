const Fonts = {
  AvenirNextLTProBold: 'AvenirNextLTPro-Bold',
  AvenirNextLTProBoldcn: 'AvenirNextLTPro-BoldCn',
  AvenirNextLTProDemi: 'AvenirNextLTPro-Demi',
  AvenirNextLTProDemicn: 'AvenirNextLTPro-Demicn',
  AvenirNextLTProIt: 'AvenirNextLTPro-It',
  AvenirNextLTProRegular: 'AvenirNextLTPro-Regular'
}

export default Fonts