import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Fonts from '../consts/Fonts'

const HeaderDetail = ({ onPress }) => {
  return (
    <View style={styles.containerStyle}>
      <View style={styles.containerLeftStyle}>
        <TouchableOpacity onPress={onPress}>
          <Image
            source={require('../images/back.png')}
            style={styles.backStyle}
          />
        </TouchableOpacity>
        <Text style={styles.titleStyle}>Detail Listing #80889</Text>
      </View>
      <Image
        source={require('../images/like.png')}
        style={styles.backStyle}
        resizeMode='contain'
      />
    </View>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    height: 50,
    paddingHorizontal: 8,
    alignItems: 'center'
  },
  containerLeftStyle: {
    flexDirection: 'row'
  },
  backStyle: {
    width: 20,
    height: 20
  },
  titleStyle: {
    fontFamily: Fonts.AvenirNextLTProDemi,
    fontSize: 16,
    marginLeft: 8
  }
})

export default HeaderDetail
