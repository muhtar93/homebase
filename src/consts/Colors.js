const Colors = {
  ACTIVE_TAB: '#BEAF87',
  IN_ACTIVE_TAB: '#242426',
  LINE: '#979797'
}

export default Colors