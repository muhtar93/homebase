import Types from '../consts/Types'
import axios from 'axios'
import Uri from '../consts/Uri'

export const onPushNotif = (token) => {
  return (dispatch) => {
    dispatch(onPushNotifLoading())
    axios.post(Uri.BASE_URL,
      {
        "to": token,
        "notification": {
          "body": "Listing properti anda berhasil dibagikan!",
          "title": "Promosikan Listing",
          "content_available": true,
          "notification_type": "Dinas",
          "priority": "high"
        }
      }, {
      headers: {
        Authorization: 'key=AAAAa1m-1sk:APA91bFZZw4c98gBO71_71oStbJDZo8fC4xqIKmGsO1riwf0B5nnP0G1jAHGBW0RDS2hpOzexmY49yFNzXeGhntE6LWTk7WtFX-fSBN4j0U7wY8QeQNtRG75ci-f0yLMzeMcFRcrLuKj'
      }
    })
      .then((result) => {
        console.log('result', result)
        dispatch(onPushNotifSuccess(result.data))
      })
      .catch((error) => {
        console.log('error', error)
        dispatch(onPushNotifError(error))
      })
  }
}

export const resetPushNotif = () => {
  return {
    type: Types.PUSH_NOTIF_RESET
  }
}

const onPushNotifLoading = () => {
  return {
    type: Types.PUSH_NOTIF_LOADING
  }
}

const onPushNotifSuccess = (result) => {
  return {
    type: Types.PUSH_NOTIF_SUCCESS,
    result
  }
}

const onPushNotifError = (error) => {
  return {
    type: Types.PUSH_NOTIF_ERROR,
    error
  }
}
